from .. conf.settings import *     


DATABASES = {  
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', 
         'NAME': os.path.join(ROOT_PATH, '../../../content/db/dev1.sqlite'),                     
         'USER': '',
         'PASSWORD': '',
         'HOST': '',
         'PORT': '',
    }, 
} 


ADMINS = (
    ('Developer', 'developer@company.com'),
)

MANAGERS = (
    ('Manager', 'manager@company.com'),
) 
