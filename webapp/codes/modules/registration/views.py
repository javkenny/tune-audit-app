from django.shortcuts import render
from .models import UserProfile
from django.views import generic
from django.utils import timezone
from django.http import HttpResponseRedirect

from django.shortcuts import get_object_or_404, render, render_to_response
from django.core.urlresolvers import reverse
from .forms import RegistrationForm
from django.core.context_processors import csrf
from django.template import RequestContext


from django.core.mail import send_mail
from django.views.generic.base import TemplateView
from django.contrib.auth import authenticate, login ,logout
from django.contrib.auth.models import User

import hashlib, datetime, random






# Create your views here.












def ta_register(request):
    args ={}
    args.update(csrf(request))
    if request.method == 'POST':

        form = RegistrationForm(request.POST)
        args['form'] = form

        if form.is_valid():
            form.save()

            username = form.cleaned_data['username']
            email = form.cleaned_data['email']
            salt = hashlib.sha1(str(random.random())).hexdigest()[:5] 
            activation_key = hashlib.sha1(salt+email).hexdigest()            
            key_expires = datetime.datetime.today() + datetime.timedelta(2)

            #Get user by username
            user=User.objects.get(username=username)

            # Create and save user profile                                                                                                                                  
            new_profile = UserProfile(user=user, activation_key=activation_key, 
                key_expires=key_expires)
            new_profile.save()

            # Send email with activation key
            email_subject = 'Account confirmation'
            email_body = "Hey %s, thanks for signing up. To activate your account, click this link within \
            48hours http://www.tuneaudit.com/tuneaudit/confirmed/%s" % (username, activation_key)

            send_mail(email_subject, email_body, 'kenny.selorm@gmail.com',
                [email], fail_silently=False)

            return HttpResponseRedirect('/tuneaudit/thanks/')

    else:
        args['form'] = RegistrationForm()
       
    return render_to_response('registration/register.html', args, context_instance=RequestContext(request))
    #return render(request, 'tuneaudit/register.html', {'form': user_form})

  
def register_confirm(request, activation_key):
    #check if user is already logged in and if he is redirect him to some other url, e.g. home
    if request.user.is_authenticated():
        HttpResponseRedirect('/tuneaudit/home/')

    # check if there is UserProfile which matches the activation key (if not then display 404)
    user_profile = get_object_or_404(UserProfile, activation_key=activation_key)

    #check if the activation key has expired, if it hase then render confirm_expired.html
    if user_profile.key_expires < timezone.now():
        return render_to_response('tuneaudit/expired.html')
    #if the key hasn't expired save user and set him as active and render some template to confirm activation
    user = user_profile.user
    user.is_active = True
    user.save()
    return render_to_response('registration/confirmed.html')
