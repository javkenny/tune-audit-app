from django.shortcuts import render_to_response,render
from django.template import RequestContext


def index(request):
    
    return render(request, 'tracks/index.html')