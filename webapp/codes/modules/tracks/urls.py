from django.conf.urls import patterns, include, url

from django.views.generic.base import TemplateView

from . import views


from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


from django.contrib import admin
admin.autodiscover()



urlpatterns = patterns('',
    url(r'^musichome/$', views.index, name="home"),
    
    
    
)

if settings.DEBUG:
    urlpatterns += patterns('',
            (r'^media/(?P<path>.*)$', 'django.views.static.serve', 
                {'document_root': settings.MEDIA_ROOT}),)
    urlpatterns += staticfiles_urlpatterns()



