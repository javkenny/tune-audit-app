from django.conf.urls import patterns, include, url

from django.views.generic.base import TemplateView

from . import views

from views import DisplayRes,DisplayHome,DisplaySorry


from django.contrib import admin
admin.autodiscover()






urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^loggedout/$', views.logout_view, name='logoutview'),
    #url(r'^tunes/$', views.get_tunes, name='tunesview'),
    #url(r'^login/$', views.ta_login, name='loginview'),
    #url(r'^emailentry/$', views.ta_email, name='emailview'),
    url(r'^restricted/', views.home, name='homepage'),
    #url(r'^login/1$', views.ta_login, name='loginview'),


    
    
    

    url(r'^name/$', views.get_name, name='nameview'),

    url(r'^thanks/$', DisplayRes.as_view()),
    url(r'^home/$', DisplayHome.as_view(),name='home'),
    url(r'^sorry/$', DisplaySorry.as_view()),

    #url(r'^user/password/', include(password_change_patterns)),


    #LOGIN
    url(r'^login/$', 'django.contrib.auth.views.login',name="my_login"),


    #--------------------------password reset ---------------------------#
    url(r'^user/password/reset/$', 'django.contrib.auth.views.password_reset', 
        {'post_reset_redirect' : '/tuneaudit/user/password/reset/done/'},
        name='password_reset'),

    url(r'^user/password/reset/done/$',
        'django.contrib.auth.views.password_reset_done'),

    url(r'^user/password/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', 'django.contrib.auth.views.password_reset_confirm',
        {'template_name': 'registration/password_reset_confirm.html',
        'post_reset_redirect' : '/tuneaudit/user/password/done/'},
        name='password_reset_confirm'),

   
    url(r'^user/password/done/$', 
        'django.contrib.auth.views.password_reset_complete'),




    

]





