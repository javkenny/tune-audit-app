from django.contrib import admin

from .models import Genre,Choice,TuneAuditUser





class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3

class GenreAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['genre_name']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('genre_name', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['genre_name']





admin.site.register(Genre,GenreAdmin)

admin.site.register(TuneAuditUser)

