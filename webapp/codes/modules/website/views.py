from django.shortcuts import render
from .models import Genre,Choice
from django.views import generic
from django.utils import timezone
from django.http import HttpResponseRedirect,HttpResponse

from django.shortcuts import get_object_or_404, render, render_to_response
from django.core.urlresolvers import reverse
from .forms import ContactForm,TuneAuditUserForm,LoginForm,RegistrationForm,EmailEntryForm
from django.core.context_processors import csrf
from django.template import RequestContext


from django.core.mail import send_mail
from django.views.generic.base import TemplateView
from django.contrib.auth import authenticate, login ,logout
from django.contrib.auth.models import User

import hashlib, datetime, random

from django.contrib.auth.decorators import login_required






# Create your views here.


@login_required
def home(request):
    return render(request, 'website/home.html')




def index(request):
    return render(request, 'website/index.html')






class DisplayRes(TemplateView):
    template_name = "website/thanks.html"



class DisplayHome(TemplateView):
    template_name = "website/home.html"    

class DisplaySorry(TemplateView):
    template_name = "tuneaudit/sorry.html"          




def logout_view(request):
    logout(request)

    return render(request, 'website/index.html')
   






  








def get_name(request):
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = ContactForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            sender = form.cleaned_data['sender']
            cc_myself = form.cleaned_data['cc_myself']

            recipients = ['kenny.selorm@gmail.com']
            if cc_myself:
                recipients.append(sender)

            send_mail(subject, message, sender, recipients)
            return HttpResponseRedirect('/tuneaudit/thanks/')
            


    # if a GET (or any other method) we'll create a blank form
    else:
        form = ContactForm()

    return render(request, 'tuneaudit/name.html', {'form': form})




