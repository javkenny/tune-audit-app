import datetime

from django.db import models

from django.utils import timezone

from django.forms import ModelForm

from django.contrib.auth.models import User




# Create your models here.
class Genre(models.Model):
    genre_name = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')
    def __unicode__(self):              # __unicode__ on Python 2
        return self.genre_name
    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now
    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'      



    def test_was_published_recently_with_old_pubdate(self):
        """
        was_published_recently() should return False for questions whose
        pub_date is older than 1 day.
        """
        time = timezone.now() - datetime.timedelta(days=30)
        old_genre = Genre(pub_date=time)
        self.assertEqual(old_genre.was_published_recently(), False)

    def test_was_published_recently_with_recent_pubdate(self):
        """
        was_published_recently() should return True for questions whose
        pub_date is within the last day.
        """
        time = timezone.now() - datetime.timedelta(hours=1)
        recent_genre = Genre(pub_date=time)
        self.assertEqual(recent_genre.was_published_recently(), True)  


class Choice(models.Model):
    genre = models.ForeignKey(Genre)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    def __unicode__(self):              # __unicode__ on Python 2
        return self.choice_text





#TUNE AUDIT MODELS.
#-------------------------------------------------------------------------------------------------------
class TuneAuditUser(models.Model):
    firstname = models.CharField(max_length=50)
    lastname = models.CharField(max_length=50)
    nickname = models.CharField(max_length=50)
    email = models.EmailField()
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    
    def __unicode__(self):              # __unicode__ on Python 2
        return self.firstname

#-------------------------------------------------------------------------------------------------------




