from django.conf.urls import include, url,patterns
from django.contrib import admin
import audiotracks

from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    # Examples:
    # url(r'^$', 'musicapps.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', 'website.views.index', name='index'),
    url(r'^tuneaudit/', include('tuneaudit.urls', namespace="tuneaudit")),
    url(r'^tuneaudit/', include('registration.urls',namespace="registration")),
    url(r'^tuneaudit/', include('website.urls', namespace="website")),
    url(r'^tuneaudit/', include('tracks.urls', namespace="tracks")),
    #url(r'^tuneaudit/', include('audiotracks.urls', namespace="audiotracks")),
    url(r'^admin/', include(admin.site.urls)),

    url("^music", include("audiotracks.urls")),
    url("^(?P<username>[\w\._-]+)/music", include("audiotracks.urls")),
]

if not settings.DEBUG:
    urlpatterns += patterns('',
        (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    )