Django==1.8.3
Pillow==2.8.1
amqp==1.4.6
anyjson==0.3.3
billiard==3.3.0.20
celery==3.1.18
dj-database-url==0.3.0
dj-static==0.0.6
django-audiofield==0.6.4
django-audiotracks==0.2.4
django-celery==3.1.16
django-toolbelt==0.0.1
django-widget-tweaks==1.3
gunicorn==19.3.0
kombu==3.0.26
mutagen==1.22
psycopg2==2.6.1
pytz==2015.4
static3==0.6.1
